# В работе...

Данный проект создан для облегчения жизни студентов СППО или тех, кто собирается поступить на это направление.
В каждой папке содержится описание предмета (ответы на доп вопросы с лаб, разъяснения по непонятным моментам, решения
проблем, которые могут возникнуть в ходе обучения, и др.), а также лабораторные / контрольные / домашние работы,
собранные разными обучающимися, которые решили внести вклад в развитие проекта, за время обучения на этом прекрасном
направлении.

Также попробуйте - https://t.me/vt_itmo_bot

## Оглавление

1. [Дисклеймер](#disclaimer)
2. [Лекторы / практики](#teachers)
3. [Полезные репозитории](#links)
4. [Лицензия](#license)
5. [Заключение](#conclusion)

## Дисклеймер <a name="disclaimer"></a>

Будьте осторожны, возможны ошибки и неточности!

<details>
<summary><b>Пояснения к названиям папок</b></summary>

- Viacheslav и Limpex - в каждой такой папке лежат лабы двух разных людей
- BCS/OPD - ОПД - Основы профессиональной деятельности
- Databases - бд - Базы данных
- Discra-base - Дискретная математика (базовый уровень)
- History - История
- Informatics - Информатика
- Kik - коммуникации и командообразование
- Life_safety - бжд - Безопасность жизнедеятельности
- Linal-base - Линейная алгебра (базовый уровень)
- Matan-base - Математический анализ (базовый уровень)
- Programming - Программирование (на java)
- 1/2 term - 1/2 семестр

</details>

## Лекторы / практики  <a name="teachers"></a>

[Гугл таблица с отзывами на преподов](https://docs.google.com/spreadsheets/d/1TFTOKxqml1agwgo6Vp0Ql6Rgj9f9ciyOqQPF8VvUkJQ/edit#gid=591156939)

## Полезные репозитории <a name="links"></a>

| Полезность                                                                                                                                                                                                                                                                                                                                | Описание                                    |
|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------|
| [Гайд для перваков](https://github.com/Imtjl/1st-year-guide)                                                                                                                                                                                                                                                                              | Просто офигенный гайд                       |
| [RedGry](https://github.com/RedGry/ITMO), [EgorMIt](https://github.com/EgorMIt/ITMO), [maxbarsukov](https://github.com/maxbarsukov/itmo), [RomanVassilchenko](https://github.com/RomanVassilchenko/ITMOProjects), [kuchizu](https://github.com/kuchizu/ItmoLabs/), [eren](https://github.com/eren24r?tab=repositories), [inertmao](https://gitlab.com/inertmao/itmo)                                                                                                                          | Популярные репозитории с работами           |
| [Black Rider](https://github.com/eliteSufferer/ITMO_Studies), [kamil](https://github.com/pro100kamil/itmo/), [belovlaska](https://github.com/belovlaska/itmo), [CodeAxeAttacks](https://github.com/CodeAxeAttacks/SystemApplicationSoftware-09.03.04-ITMO), [VeraKasianenko](https://github.com/VeraKasianenko/ITMO_Software_engineering) | Репозитории других ребят (поступили в 2022) |
| [Vaneshik](https://github.com/Vaneshik/VT-Labs), [bilyardvmetro](https://github.com/bilyardvmetro/ITMO-System-Application-Software)                                                                                                                                                                                                       | Репозитории других ребят (поступили в 2023) |
| https://205826.github.io                                                                                                                                                                                                                                                                                                                  | Легенда                                     |
| [MakeCheerfulUpload](https://github.com/orgs/MakeCheerfulUpload/repositories)                                                                                                                                                                                                                                                             | Сборник работ 1 сема                        |
| https://rosroble.github.io/                                                                                                                                                                                                                                                                                                               | Конспекты к лабам                           |
| https://picloud.pw/cloud/                                                                                                                                                                                                                                                                                                                 | Хранилище работ до 2020 года                |

Спасибо всем тем, на кого указаны ссылки здесь и также в других папках.

## Заключение <a name="conclusion"></a>

> [!NOTE]
> <b>Если вы нашли для себя что-то полезное в данном репозитории, поставьте пж звезду :star:</b><br>
